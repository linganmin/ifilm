<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



//    Route::get('/films','FilmController@index');
//    Route::get('/films/{id}','FilmController@show');
//    Route::get('/search','FilmController@search');
Route::get('index',function (){
    $movies = \App\Models\Film::whereTag(1)->limit(16)->get();
    $tvs = \App\Models\Film::whereTag(2)->limit(16)->get();
    $animes = \App\Models\Film::whereTag(3)->limit(16)->get();

    return compact('movies','tvs','animes');
});

Route::get('films/{id}',function ($id){
    return \App\Models\Film::whereId($id)->first();
});


Route::get('films',function (){
    return \App\Models\Film::whereTag(1)->paginate(20);
});

Route::get('tvs',function (){
    return \App\Models\Film::whereTag(2)->paginate(20);
});

Route::get('search',function (){
    $q = \Request::input('q');
    $movies = \App\Models\Film::where('title','like','%'.$q.'%')->whereTag(1)->get();
    $tvs = \App\Models\Film::where('title','like','%'.$q.'%')->whereTag(2)->get();

    return compact('movies','tvs');
});