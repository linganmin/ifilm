## a base project by laravel5.4 install some package

### installed packages list

- barryvdh/laravel-cors 
- barryvdh/laravel-ide-helper 
- doctrine/dbal 
- mpociot/laravel-apidoc-generator 
- mpociot/laravel-test-factory-helper 
- overtrue/laravel-lang 
- overtrue/laravel-socialite 
- overtrue/laravel-wechat 
- predis/predis 
- tymon/jwt-auth 
- yangyifan/upload 