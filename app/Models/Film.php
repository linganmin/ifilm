<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Notifications\Notifiable;

class Film extends Model
{


    public $fillable = ['title', 'desc', 'cover', 'src', 'tag'];

    public $casts = [];

    public $appends = [];

    public $dates = [];


    public function getSrcAttribute($value)
    {
        return explode(',',$value);

    }


}
