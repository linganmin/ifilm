<?php
/**
 * Created by PhpStorm.
 * User: saboran
 * Date: 2017/5/6
 * Time: 10:57
 */


function getEncode($data)
{

// 定义变量
    $key_base = "contentWindowHig";
    $iv_base = "contentDocuments";

// 加密前处理
    $key = md5($key_base);
    $iv = $iv_base;

// 加密
    $cryptText = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);
    return urlencode(base64_encode($cryptText));
}

/**
 * @param $client
 * @param $baseUrl
 * @param $matchs
 * @return array
 */
function getVideoSrcs($client, $baseUrl, $playPageUrls,$isVideo = false)
{
    $data = [];
    foreach ($playPageUrls[1] as $k => $playPageUrl) {
        if($isVideo && strpos($playPageUrl,'1.html')){
            continue;
        }
        $playPageUrl = $baseUrl . $playPageUrl;
        $playPage = $client->get($playPageUrl);
        $playPageContent = $playPage->getBody();
        $regexJs = '/eval\(fun.+/';
        preg_match($regexJs, $playPageContent, $matchJs);

        $js = JavaScriptUnpacker::unpack($matchJs[0]); // 解密

        $regexSrc = '/src=\"(.+?)\"/';
        preg_match($regexSrc, $js, $matchSrc);
        $data[] = $matchSrc[1];
    }

    return $data;

}

