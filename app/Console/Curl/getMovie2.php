<?php

/**
 * 通过分页获取电影资源，神马电影
 */
require_once('../../../public/index.php');

// 实例化Http
$client = new \GuzzleHttp\Client();


$baseUrl = "http://m.renren66.com/";


$i = 0;

while (++$i){
    print_r('
page'.$i
    );
    if($i == 1){
        $uri = 'ldy.html';
    }else{
        $uri = 'ldy_'.$i.'.html';
    }

    $firstPage = $client->get($baseUrl . $uri);
    $firstPageContent = $firstPage->getBody();

    $regextitle = '/ui-pic.+?title=\"(.+?)\"/';
    preg_match_all($regextitle, $firstPageContent, $matchTitles);

    // 当匹配不到时，停止
    if(empty($matchTitles[1])){
        print_r('
END'
        );
        die;
    }

    $regexCover = '/ui-pic.+?img.+?src=\"(.+?)\"/';
    preg_match_all($regexCover, $firstPageContent, $matchCovers);

    $regexPlayUrl = '/ui-pic.+?href=\"(.+?html)\"/';
    preg_match_all($regexPlayUrl, $firstPageContent, $playPageUrls);

    $srcs = getVideoSrcs($client, $baseUrl, $playPageUrls);

    if (count($matchTitles[1]) == count($srcs) && count($matchTitles[1]) == count($matchCovers[1])) {
        foreach ($srcs as $k => $v) {
            $temp['title'] = $matchTitles[1][$k];
            $temp['cover'] = $matchCovers[1][$k];
            $temp['src'] = $v;
            $temp['tag']=1;

            \App\Models\Film::create($temp);
        }


    } else {
        print_r('
ERROR'
        );
    }

}
