<?php

/**
 * 通过分页获取电影资源，最新电影
 */
require_once('../../../public/index.php');

// 实例化Http
$client = new \GuzzleHttp\Client();


$baseUrl = "http://m.renren66.com/";


$i = 0;

while (++$i) {
    print_r('
page' . $i
    );
    if ($i == 1) {
        $uri = 'vip.html';
    } else {
        $uri = 'vip_' . $i . '.html';
    }

    $firstPage = $client->get($baseUrl . $uri);
    $firstPageContent = $firstPage->getBody();

    $regextitle = '/ui-pic.+?title=\"(.+?)\"/';
    preg_match_all($regextitle, $firstPageContent, $matchTitles);

    // 当匹配不到时，停止
    if (empty($matchTitles[1])) {
        print_r('
END'
        );
        die;
    }

    $regexCover = '/ui-pic.+?img.+?src=\"(.+?)\"/';
    preg_match_all($regexCover, $firstPageContent, $matchCovers);

    $regexUrl = '/ui-pic.+?href=\"(.+?html)\"/';
    preg_match_all($regexUrl, $firstPageContent, $matchUrls);


    // 遍历每个视频
    foreach ($matchUrls[1] as $k => $v) {
        $playPage = $client->get($baseUrl . $v);
        $playPageContent = $playPage->getBody();

        // 播放页面链接
        $regexPlayPageUrl = '/><a href=\"(.+?html)\".*?title/';
        preg_match_all($regexPlayPageUrl, $playPageContent, $matchPlayPageUrls);

        $srcs = getVideoSrcs($client, $baseUrl, $matchPlayPageUrls);

        if ($srcs) {
            \App\Models\Film::create([
                'title' => $matchTitles[1][$k],
                'cover' => $matchCovers[1][$k],
                'tag' => 1,
                'src' => implode(',', $srcs)
            ]);
        }
    }
}


print_r('
END'
);
//$i = 1787;
//while ($i < 1788) {
//    $firstPage = $client->get($baseUrl . 'movie/id_' . $i . '.html');
//    $firstPageContent = $firstPage->getBody();
//
////    file_put_contents('55.txt',$firstPageContent);
////    die();
//    // 集数
//    $regexCount = '/第.+?集/';
//    file_put_contents('55.txt', $firstPageContent);
//    preg_match($regexCount, $firstPageContent, $matchCount);
//
//    // 当不是电视剧时跳过
//    if (!isset($matchCount[0])) {
//        continue;
//    }
//    // 播放页面链接
//    $regexPlayPageUrl = '/><a href=\"(.+?html)\".*?title/';
//    preg_match_all($regexPlayPageUrl, $firstPageContent, $playPageUrls);
//
//    $srcs = getVideoSrcs($client, $baseUrl, $playPageUrls,true);
//
//
//    // 当存在链接时再去获取图片和名字
//    if (!empty($srcs)) {
//        // 名字
//        $regexTitle = '/><img alt=\"(.+?)\"/';
//        preg_match($regexTitle, $firstPageContent, $matchTitle);
//
//        // 图片
//        $regexCover = '/立即播放.+src=\"(.+)\"/';
//        preg_match($regexCover, $firstPageContent, $matchCover);
//
//
//        \App\Models\Film::create([
//            'title' => $matchTitle[1],
//            'cover' => $matchCover[1],
//            'src' => implode(',', $srcs),
//            'tag'=>2,
//        ]);
//    }
//
//    $i++;
//    print_r('
//NO.' . ($i - 1)
//    );
//}
//print_r('
//END'
//);




