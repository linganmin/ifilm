<?php

require_once('../../../public/index.php');

// 实例化Http
$client = new \GuzzleHttp\Client();


// http请求参数
$options = [
    'headers' => [
        'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding' => 'gzip, deflate, sdch',
        'Accept-Language' => 'zh-CN,zh;q=0.8',
        'Cache-Control' => 'max-age=0',
        'Connection' => 'keep-alive',
        'Referer' => 'http://www.7nmg.com',
        'Upgrade-Insecure-Requests' => '1',
        'User-Agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
    ],
    'allow_redirects' => [
        'max' => 5,
        'strict' => false,
        'referer' => false,
        'protocols' => ['http', 'https'],
        'track_redirects' => false
    ],
    'verify' => false
];


$start = env('FILM_START'); // 开始id
$end = env('FILM_END'); // 结束id
$failPosition = [];

// 循环执行
while ($start <= $end) {

    // 获取标题和封面
    $firstPage = $client->request('GET', 'http://www.7nmg.com/show/' . $start . '.html', $options);
    $firstPageContent = $firstPage->getBody();

    // 正则
    $regexTitle = '/dt class=\"name">(.*)<span/';
    $regexCover = '/<div class=\"ct-l\">.+src=\"(.+jpg)\"/';
    $regexDesc = '/介绍.+>(.+)/';
    $regexCount = "/<li.+第(.+)集/";
    $regexCount2 = "/<li.+第(.+)期/";

    preg_match($regexTitle, $firstPageContent, $matches);
    $title = isset($matches[1]) ? $matches[1] : null;

    preg_match($regexCover, $firstPageContent, $matches);
    $cover = isset($matches[1]) ? $matches[1] : null;

    preg_match($regexDesc, $firstPageContent, $matches);
    $desc = isset($matches[1]) ? $matches[1] : null;

    preg_match($regexCount, $firstPageContent, $matches);
    $count = isset($matches[1]) ? $matches[1] : null;

    preg_match($regexCount2, $firstPageContent, $matches);
    $count2 = isset($matches[1]) ? $matches[1] : null;



    // 获取展示网站内容
    $secondPage = $client->request('GET', 'http://www.7nmg.com/play/' . $start . '/1/1.html', $options);
    $secondPageContent = $secondPage->getBody();

    // 判断是否失败
    if (strpos($secondPageContent, 'location.href')) {

        $failPosition[] = $start;
        // 当连续失败20次则停止
        if (count($failPosition) > 200) {
            file_put_contents('./fail-position.txt', implode('|', $failPosition));
            print_r('
---------------------------------------------
---------------------------------------------
-----------------FINISHED--------------------
---------------------------------------------
---------------------------------------------

');
            die;
        }

        // 变量加1，跳出此次循环
        ++$start;
        continue;

    }

    // 不连续则清空失败数组
    $failPosition = [];

    // 判断集数是否存在集数，若存在按照电视剧处理
    if (!isset($count) && !isset($count2)) {
        // 处理没有集数的视频
        $video = getUrl($client, $options, $secondPageContent, $start);


        if (isset($video)) {
            \App\Models\Film::create([
                'title' => $title,
                'cover' => $cover,
                'desc' => $desc,
                'video' => $video
            ]);
        }

    } else {
        $count = isset($count) ? $count : $count2;


        // 当集数超过100跳过
        if ($count > 100){
            $start ++;
            continue;
        }

        // 处理有集数的
        $videos = [];  // 链接

        // 遍历集数
        for ($k = 1; $k <= $count; $k++) {
            // 请求当前剧目的子集页面内容
            $thirdPage = $client->request('GET', 'http://www.7nmg.com/play/' . $start . '/1/' . $k . '.html', $options);
            $thirdPageContent = $thirdPage->getBody();

            // 调用函数获取视频链接
            $video = getUrl($client, $options, $thirdPageContent, $start);
            $videos[] = $video;

        }

        // 判断标题和视频是否为空，为空不写入数据库
        if (isset($title) && isset($videos[0])) {
            \App\Models\Film::create([
                'title' => $title,
                'cover' => $cover,
                'desc' => $desc,
                'video' => implode(',', $videos)
            ]);
        }
    }

    $start++;

    print_r('
NO.' . $start);
}


print_r('
---------------------------------------------
---------------------------------------------
-----------------FINISHED--------------------
---------------------------------------------
---------------------------------------------

');

/**
 * 获取视频链接
 * @param $client
 * @param $options
 * @param $secondPageContent
 * @param $id
 * @return array
 */
function getUrl($client, $options, $secondPageContent, $id)
{
    // 正则规则链接地址
    $regexURL = "/<iframe.*src=\"(.*?)\"/";
    // 匹配iframe视频地址
    preg_match($regexURL, $secondPageContent, $matches);
    $url = isset($matches[1]) ? $matches[1] : null;

    // 获取播放页链接
    if (isset($url)) {

        // http请求地址，模拟视频网站
        try {
            $filmPage = $client->request('GET', $url, $options);
            // 写入临时文件
            file_put_contents('./temp.txt', $filmPage->getBody());

            // 判断是否可以直接在video标签播放
            if (!strpos(file_get_contents('./temp.txt'), "$.post")) {
                $regex = "/<video.*src=\"(.*?)\"/";
                preg_match($regex, $filmPage->getBody(), $matches);
                $videoUrl = isset($matches[1]) ? $matches[1] : null;

                // 过滤掉yy，404，空的视频地址
                if (!mb_strpos($videoUrl, 'yy.com') && !mb_strpos($videoUrl, '404.php') && !empty($videoUrl)) {
                    return $videoUrl;
                }
            } else {
                // 处理其他
            }
        } catch (\Exception $exception) {
            file_put_contents('./error.txt', $id . '--' . PHP_EOL, FILE_APPEND);
        }

    } else {
        file_put_contents('./error.txt', $id . '--' . PHP_EOL, FILE_APPEND);
    }

}
