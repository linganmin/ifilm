<?php

/**
 * 通过分页获取电视剧资源，破晓电视剧
 */
require_once('../../../public/index.php');

// 实例化Http
$client = new \GuzzleHttp\Client();


$baseUrl = "http://m.renren66.com/";


$i = 0;

while (++$i) {
    print_r('
page' . $i
    );
    if ($i == 1) {
        $uri = 'mtv.html';
    } else {
        $uri = 'mtv_' . $i . '.html';
    }

    $firstPage = $client->get($baseUrl . $uri);
    $firstPageContent = $firstPage->getBody();

    $regextitle = '/ui-pic.+?title=\"(.+?)\"/';
    preg_match_all($regextitle, $firstPageContent, $matchTitles);

    // 当匹配不到时，停止
    if (empty($matchTitles[1])) {
        print_r('
END'
        );
        die;
    }

    $regexCover = '/ui-pic.+?img.+?src=\"(.+?)\"/';
    preg_match_all($regexCover, $firstPageContent, $matchCovers);

    $regexUrl = '/ui-pic.+?href=\"(.+?html)\"/';
    preg_match_all($regexUrl, $firstPageContent, $matchUrls);

    // 遍历每个视频
    foreach ($matchUrls[1] as $k => $v) {
        $playPage = $client->get($baseUrl . $v);
        $playPageContent = $playPage->getBody();

        file_put_contents('55.txt',$playPageContent);
        // 播放页面链接
        $regexPlayPageUrl = '/<a href=\"(\/play.+?.html)\"/';
        preg_match_all($regexPlayPageUrl, $playPageContent, $matchPlayPageUrls);
        

        $srcs = getVideoSrcs($client, $baseUrl, $matchPlayPageUrls,true);

        if ($srcs && strpos($srcs[0],'ttp')) {
            \App\Models\Film::create([
                'title' => $matchTitles[1][$k],
                'cover' => $matchCovers[1][$k],
                'tag' => 2,
                'src' => implode(',', $srcs)
            ]);
        }
    }
}


print_r('
END'
);




