<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Film
 *
 * @property int $id
 * @property string $title 名字
 * @property string $desc 描述
 * @property string $cover 封面
 * @property string $video 视频
 * @property string $tag 标签
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereCover($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereTag($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Film whereVideo($value)
 */
	class Film extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
	class User extends \Eloquent {}
}

